﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Pabii.Models;

namespace Pabii
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Products>().Property(x => x.Id).HasDefaultValueSql("NEWSEQUENTIALID()");
            modelBuilder.Entity<Users>().Property(x => x.Id).HasDefaultValueSql("NEWSEQUENTIALID()");
        }

        public DbSet<Products> Products { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}
