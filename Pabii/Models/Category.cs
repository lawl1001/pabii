﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pabii.Models
{
    public class Category
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryEnum CategoryEnumeration { get; set; }
    }

    public enum CategoryEnum
    {
        [Display(Name = "Electronic Device")]
        ElectronicDevice,

        [Display(Name = "Electronic Accessories")]
        ElectronicAccessories,

        [Display(Name = "Appliances")]
        Appliances,

        [Display(Name = "Furniture")]
        Furniture,

        [Display(Name = "Men's Fashion")]
        MensFashion,

        [Display(Name = "Women's Fashion")]
        WomensFashion,
    }
}
