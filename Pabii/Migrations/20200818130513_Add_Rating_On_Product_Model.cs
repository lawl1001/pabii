﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Pabii.Migrations
{
    public partial class Add_Rating_On_Product_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TotalRaters",
                table: "Products",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalRating",
                table: "Products",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalRaters",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TotalRating",
                table: "Products");
        }
    }
}
