﻿// FORM LOAD
$(document).ready(function () {
    LoadItems();
});

// COMBOBOX ITEM CHANGE
$("#CategoryEnumeration").on("change", function () {
    var category = document.getElementById("CategoryEnumeration").value;
    if (category >= 0 && category != "") {
        LoadItems(category);
    }
});

function LoadItems(cat = null) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/home/getproducts',
        data: { cat: cat },
        success: function (result) {
            if (result.data.length == 0) {
                $("#div-row-contents").empty();
                $("#div-row-contents").append(`
                        <p id="no-products-p">No Products Available</p>
                `);
            }
            else {
                $("#div-row-contents").empty();
                $.each(result.data, function (index, item) {
                    CreateRow(item);
                });
               //window.location.hash = "#div-products"
            }
        }
    });
}

function CreateRow(item) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });

    var ratings = 0;
    if (item.totalRating != 0)
        ratings = parseInt(item.totalRating) / parseInt(item.totalRaters);

    var sellingprice = item.price;
    var oldprice = "";
    var percent = "";

    var datenow = new Date();

    if (item.discountDateFrom != null && item.discountDateTo != null) {
        if (datenow >= new Date(item.discountDateFrom) && datenow <= new Date(item.discountDateTo)) {
            oldprice = formatter.format(sellingprice).replace('$', '₱');
            sellingprice = item.discountedPrice;
            percent = item.discountPercent + "%";
        }
    }

    $("#div-row-contents").append(`
        <div>
            <a href="#">
                <img src="/Resources/Image/bb_2.jpg" alt="tae">
                <p>${item.name}</p>
                <p>Rating: ${Math.round(ratings * 10) / 10}/5</p>
                <p class="selling-price">${formatter.format(sellingprice).replace('$', '₱')}</p>
                <p style="font-size:small;"><span style="font-size:small;" class="original-price">${oldprice}</span>&nbsp&nbsp&nbsp${percent}</p>
            </a>
        </div>
    `);
}