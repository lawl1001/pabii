﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pabii.Models;

namespace Pabii.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;
        private readonly Context _context;

        public HomeController(Context db)
        {
            _context = db;
        }

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts(int? cat)
        {
            if (cat == null)
                return Json(new { data = await _context.Products.ToListAsync() });
            else
                return Json(new { data = await _context.Products.Where(x => x.Category == cat).ToListAsync() });
        }

        [HttpGet]
        [Route("api/[controller]/products")]
        public async Task<IActionResult> GetProducts()
        {
            return Json(new { data = await _context.Products.ToListAsync() });
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
